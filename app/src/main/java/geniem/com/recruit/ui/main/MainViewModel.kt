package geniem.com.recruit.ui.main

import android.os.Handler
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import geniem.com.recruit.ImageEntity
import geniem.com.recruit.client.ClientOkHttp


class MainViewModel : ViewModel() {
    // TODO: Implement the ViewModel
    private var imageEntitys = MutableLiveData<MutableList<ImageEntity>>()

    fun getUsers(): MutableLiveData<MutableList<ImageEntity>> {

        imageEntitys = MutableLiveData<MutableList<ImageEntity>>()

        return imageEntitys
    }

    private val clientOkHttpClient = ClientOkHttp()

    fun loadUsers() {

        val thread = Thread {

            val responce = clientOkHttpClient.getImageMetaData()

            val mutuableList = mutableListOf<ImageEntity>()

            for (x in 0..responce.length() - 1) {
                val jsonObject = responce.getJSONObject(x)

                val imageEntity = ImageEntity()

                imageEntity.url = jsonObject.getString("url")

                imageEntity.title = jsonObject.getString("title")

                imageEntity.id = jsonObject.getInt("id")

                imageEntity.thumbnailUrl = jsonObject.getString("thumbnailUrl")

                mutuableList.add(imageEntity)
            }

            Handler(Looper.getMainLooper()).post {

                imageEntitys.postValue(mutuableList)
            }
        }
        thread.start()
    }
}

