package geniem.com.recruit.ui.main.adapters

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.BaseAdapter
import android.widget.ImageView
import geniem.com.recruit.ImageEntity
import geniem.com.recruit.client.ClientOkHttp

class ImageGridViewAdapter(private val mContext: Context, val imagesList: MutableList<ImageEntity>) : BaseAdapter() {

    override fun getCount(): Int {
        return imagesList.size

    }

    override fun getItem(position: Int): Any {
        return imagesList[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val imgview = ImageView(mContext)
        imgview.layoutParams = AbsListView.LayoutParams(370, 250)
        imgview.scaleType = ImageView.ScaleType.CENTER_CROP
        imgview.setPadding(10, 10, 10, 10)

        val thread = Thread {

            val clientOkHttp = ClientOkHttp()

            val bitmap = clientOkHttp.loadImage(imagesList[position].thumbnailUrl)

            Handler(Looper.getMainLooper()).post {

                imgview.setImageBitmap(bitmap)
            }
        }

        thread.start()

        return imgview
    }

}