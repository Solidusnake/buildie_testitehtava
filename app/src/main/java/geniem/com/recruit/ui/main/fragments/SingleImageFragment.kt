package geniem.com.recruit.ui.main.fragments

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import geniem.com.recruit.ImageEntity
import geniem.com.recruit.R
import geniem.com.recruit.client.ClientOkHttp


class SingleImageFragment(val imageEntity: ImageEntity) : Fragment() {

    companion object {
        fun newInstance(imageEntity: ImageEntity) : Fragment{
            val fragment = SingleImageFragment(imageEntity)
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.image_layout, parent, false);

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val imageView = view.findViewById(R.id.image_view) as ImageView

        val textView = view.findViewById(R.id.image_title) as TextView

        val thread = Thread {

            val clientOkHttp = ClientOkHttp()

            val bitmap = clientOkHttp.loadImage(imageEntity.url)

            Handler(Looper.getMainLooper()).post {

                imageView.setImageBitmap(bitmap)
            }
        }

        thread.start()

        textView.text = imageEntity.title

    }
}
