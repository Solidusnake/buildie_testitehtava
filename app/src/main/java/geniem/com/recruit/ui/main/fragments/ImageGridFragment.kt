package geniem.com.recruit.ui.main.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import androidx.fragment.app.Fragment
import geniem.com.recruit.ImageEntity
import geniem.com.recruit.R
import geniem.com.recruit.ui.main.adapters.ImageGridViewAdapter


class ImageGridFragment(private val mContext: Context, val imageListEntity: MutableList<ImageEntity>) : Fragment() {

    companion object {
        fun newInstance( mContext: Context, imageListEntity: MutableList<ImageEntity>) : Fragment{
            val fragment = ImageGridFragment(mContext, imageListEntity)
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.images_layout, parent, false);

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val gridView = view.findViewById(R.id.grid_layout) as GridView

        gridView.adapter = ImageGridViewAdapter(mContext, imageListEntity)

        gridView.setOnItemClickListener({ parent, v, position, id -> // Send intent to SingleViewActivity

            activity?.supportFragmentManager?.beginTransaction()
                    ?.replace(R.id.container, SingleImageFragment.newInstance(imageListEntity[position]), "ImageFragment")
                    ?.commitNow()

        })
    }
}
