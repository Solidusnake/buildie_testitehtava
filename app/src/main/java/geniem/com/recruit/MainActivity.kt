package geniem.com.recruit

import android.os.Bundle
import android.view.KeyEvent
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import geniem.com.recruit.ui.main.fragments.ImageGridFragment
import geniem.com.recruit.ui.main.MainViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    private var changeObserver = Observer<MutableList<ImageEntity>> {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.main_activity)

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        // TODO: Use the ViewModel

        changeObserver = Observer<MutableList<ImageEntity>> { imageEntitysArray ->

            if (savedInstanceState == null) {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.container, ImageGridFragment.newInstance(this, imageEntitysArray), "ImagesFragment")
                        .commitNow()
            }
        }

        viewModel.getUsers().observe(this, changeObserver)

    }

    override fun onResume() {
        super.onResume()

        viewModel.loadUsers()

    }

    override fun onDestroy() {
        super.onDestroy()
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            val imagesFragment: ImageGridFragment? = supportFragmentManager.findFragmentByTag("ImagesFragment") as ImageGridFragment?
            if (imagesFragment != null && imagesFragment.isVisible()) {
                finish()
            } else {
                viewModel.loadUsers()
            }
        }
        return true
    }

}
