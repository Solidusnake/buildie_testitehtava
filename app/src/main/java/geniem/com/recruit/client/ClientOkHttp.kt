package geniem.com.recruit.client

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import java.io.IOException
import java.io.InputStream

class ClientOkHttp {

    @Throws(IOException::class)
    fun getImageMetaData(): JSONArray {
        var client = OkHttpClient()

        val request: Request = Request.Builder()
                .url("https://jsonplaceholder.typicode.com/albums/1/photos")
                .build()
        client.newCall(request).execute().use({ response ->

            var jsonData = JSONArray(response.body()?.string())

            return jsonData
        })
    }

    fun loadImage(url: String): Bitmap? {

        var client = OkHttpClient()

        val request: Request = Request.Builder()
                .url(url)
                .build()
        client.newCall(request).execute().use({ response ->

            val inputStream: InputStream = response.body()!!.byteStream()

            return BitmapFactory.decodeStream(inputStream)

        })
    }

    fun StringToBitMap(encodedString: String?): Bitmap? {
        return try {
            val encodeByte: ByteArray = Base64.decode(encodedString, Base64.DEFAULT)
            BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)
        } catch (e: Exception) {
            e.message
            null
        }
    }
}